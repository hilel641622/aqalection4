package app;

public class cylinderVolume {
    public static double calculateVolume(double radius, double height) {
        double volume = Math.PI * Math.pow(radius, 2) * height;
        return volume;
    }

    public static void main(String[] args) {
        double radius = 3.5;
        double height = 10.0;

        double volume = calculateVolume(radius, height);
        System.out.println("Об'єм циліндра з радіусом " + radius + " і висотою " + height + " дорівнює " + volume);
    }
}

//2. Об'єм циліндра з радіусом 3.5 і висотою 10.0 дорівнює 384.81334713945307.
// V = π ⋅ r 2 ⋅ h , де r — радiус основи, а h — висота цилiндра.
