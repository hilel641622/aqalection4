package app;

public class HomeWork1  {
    public static void printSquare(int number) {
        int square = number * number;
        System.out.println("Квадрат числа " + number + " дорівнює " + square);
    }

    public static void main(String[] args) {
        int number = 5;
        printSquare(number);
    }
}