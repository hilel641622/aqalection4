package app;

public class AInDegreeB {
    public static int power(int a, int b) {
        int result = 1;
        for (int i = 0; i < b; i++) {
            result *= a;
        }

        return result;
    }

    public static void main(String[] args) {
        int a = 2;
        int b = 3;

        int result = power(a, b);
        System.out.println(a + " в степені " + b + " дорівнює " + result); //
    }
}
//Введіть a: 2
//   Введіть b: 3
//   Результат 2^3 дорівнює 8.